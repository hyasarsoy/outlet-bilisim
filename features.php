<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hosting Press - VPS Hosting</title>
    
    <!--Favicons-->
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#e74c3c">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#e74c3c">
    
    <!--Bootstrap-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    
    <!--Loader-->
    <link rel="stylesheet" href="css/spinners.css">
    
    
    <!-- Vendors -->
    <link rel="stylesheet" href="vendors/owl.carousel/owl.carousel.css">
    <link rel="stylesheet" href="vendors/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="vendors/bootstrap-select/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/lineariconsFree/style.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    
    <!--Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    
    <!--Theme Styles-->
    <link rel="stylesheet" href="css/default/style.css">
    <link rel="stylesheet" href="css/responsive/responsive.css">
    
    <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
    
</head>
<body class="shortcode">
    
    <div class="preloader">
        <div class="dots-loader">Loading ...</div>
    </div>
   
    <?php include 'header.php'; ?>

    <?php include 'menu.php'; ?>
    
   
      <section class="row find_domain find_domain2 find_domain_drop collapse" id="find_domain_drop">
        <div class="container">
            <form action="#" class="row m0 domain_search">
                <div class="col-sm-3 col-md-3 col-lg-2 form_title">
                    <h5>Find Your</h5>
                    <h2>Domain</h2>
                    <p>Search for your dream domain now</p>
                </div>
                <div class="col-sm-9 col-md-9 col-lg-10">
                    <div class="input-group">
                        <input type="search" class="form-control">
                        <div class="input-group-addon">
                            <div class="searchFilters fleft">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span id="searchFilterValue">.com</span> <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a href="#com">.com</a></li>
                                    <li><a href="#.net">.net</a></li>
                                    <li><a href="#.org">.org</a></li>
                                    <li><a href="#.us">.us</a></li>
                                    <li><a href="#.biz">.biz</a></li>
                                </ul>
                            </div>
                            <input type="submit" value="search" class="">
                        </div>
                    </div>
                    <ul class="nav nav-pills price_list">
                        <li>.com $5.75</li>
                        <li>.net $9.45</li>
                        <li>.org $7.50</li>
                        <li>.us $5.99</li>
                        <li>.biz $9.99</li>
                    </ul>
                </div>
            </form>
        </div>
    </section>
    <div class="row m0 drop_icon"><a href="#find_domain_drop" class="domain_search_drop" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="find_domain_drop">+</a></div>
    <section class="row page_header">
        <div class="container">
            <h3>features</h3>
            <ol class="breadcrumb">
                <li><a href="index.html">home</a></li>
                <li class="active">features</li>
            </ol>
        </div>
    </section>
    
  
    
    <section class="row featureTab">
        <div class="row feature_tab_menu m0">
            <div class="container">
                <div class="row">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#featureT1" aria-controls="featureT1" role="tab" data-toggle="tab">
                            <img src="images/icons/feature/1.png" alt="" class="icon">technology
                        </a></li>
                        <li role="presentation"><a href="#featureT2" aria-controls="featureT2" role="tab" data-toggle="tab">
                            <img src="images/icons/feature/2.png" alt="" class="icon">api
                        </a></li>
                        <li role="presentation"><a href="#featureT3" aria-controls="featureT3" role="tab" data-toggle="tab">
                            <img src="images/icons/feature/3.png" alt="" class="icon">control <br> panel
                        </a></li>
                        <li role="presentation"><a href="#featureT4" aria-controls="featureT4" role="tab" data-toggle="tab">
                            <img src="images/icons/feature/4.png" alt="" class="icon">distros &amp; <br> 1 click apps
                        </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row m0 featureTab_contents">
            <div class="tab-content">
                <div class="tab-pane active" role="tabpanel" id="featureT1">
                    <div class="content_row row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>technology</h5>
                                <h2>ssd-only cloud</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-7">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. </p>
                                    <a href="#" class="btn btn-primary">get started</a>
                                </div>
                                <div class="col-sm-5 img_part">
                                    <img src="images/icons/feature/5.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row2 whats_under_hood row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>technology</h5>
                                <h2>what's under the hood?</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/7.png" alt=""></div>
                                    <h5>55 Second Provisioning</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/8.png" alt=""></div>
                                    <h5>ssd hard drives</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/9.png" alt=""></div>
                                    <h5>simple control panel</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/10.png" alt=""></div>
                                    <h5>kvm virtualization</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/11.png" alt=""></div>
                                    <h5>tier-1 bandwidth</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/12.png" alt=""></div>
                                    <h5>amazing hardware</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row3 row m0">
                        <div class="container">
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>Technology</h4>
                                    <h3>private networking</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/13.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>Technology</h4>
                                    <h3>simple api</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/14.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>Technology</h4>
                                    <h3>ipv6 support</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/15.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content_row  whats_under_hood row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>technology</h5>
                                <h2>transfer globally</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 img_part">
                                    <img src="images/icons/feature/6.png" alt="">
                                </div>
                                <div class="col-sm-6">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. </p>
                                    <a href="#" class="btn btn-primary">get started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" role="tabpanel" id="featureT2">
                    <div class="content_row row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>api</h5>
                                <h2>ssd-only cloud</h2>
                            </div>
                            <div class="row">
                             <div class="col-sm-5 img_part">
                                    <img src="images/icons/feature/5.png" alt="">
                                </div>
                                <div class="col-sm-7">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. </p>
                                    <a href="#" class="btn btn-primary">get started</a>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row2 whats_under_hood row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>api</h5>
                                <h2>what's under the hood?</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/7.png" alt=""></div>
                                    <h5>55 Second Provisioning</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/8.png" alt=""></div>
                                    <h5>ssd hard drives</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/9.png" alt=""></div>
                                    <h5>simple control panel</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/10.png" alt=""></div>
                                    <h5>kvm virtualization</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/11.png" alt=""></div>
                                    <h5>tier-1 bandwidth</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/12.png" alt=""></div>
                                    <h5>amazing hardware</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row3 row m0">
                        <div class="container">
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>api</h4>
                                    <h3>private networking</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/13.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>Technology</h4>
                                    <h3>simple api</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/14.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>Technology</h4>
                                    <h3>ipv6 support</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/15.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row4 whats_under_hood row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>api</h5>
                                <h2>transfer globally</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 img_part">
                                    <img src="images/icons/feature/6.png" alt="">
                                </div>
                                <div class="col-sm-6">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. </p>
                                    <a href="#" class="btn btn-primary">get started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" role="tabpanel" id="featureT3">
                    <div class="content_row row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>control panel</h5>
                                <h2>ssd-only cloud</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-7">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. </p>
                                    <a href="#" class="btn btn-primary">get started</a>
                                </div>
                                <div class="col-sm-5 img_part">
                                    <img src="images/icons/feature/5.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row2 whats_under_hood row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>control panel</h5>
                                <h2>what's under the hood?</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/7.png" alt=""></div>
                                    <h5>55 Second Provisioning</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/8.png" alt=""></div>
                                    <h5>ssd hard drives</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/9.png" alt=""></div>
                                    <h5>simple control panel</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/10.png" alt=""></div>
                                    <h5>kvm virtualization</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/11.png" alt=""></div>
                                    <h5>tier-1 bandwidth</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/12.png" alt=""></div>
                                    <h5>amazing hardware</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row3 row m0">
                        <div class="container">
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>control panel</h4>
                                    <h3>private networking</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/13.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>Technology</h4>
                                    <h3>simple api</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/14.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>Technology</h4>
                                    <h3>ipv6 support</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/15.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row4 row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>control panel</h5>
                                <h2>transfer globally</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 img_part">
                                    <img src="images/icons/feature/6.png" alt="">
                                </div>
                                <div class="col-sm-6">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. </p>
                                    <a href="#" class="btn btn-primary">get started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" role="tabpanel" id="featureT4">
                    <div class="content_row row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>distros &amp; 1 click apps</h5>
                                <h2>ssd-only cloud</h2>
                            </div>
                            <div class="row">
                             <div class="col-sm-5 img_part">
                                    <img src="images/icons/feature/5.png" alt="">
                                </div>
                                <div class="col-sm-7">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. </p>
                                    <a href="#" class="btn btn-primary">get started</a>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row2 whats_under_hood row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>distros &amp; 1 click apps</h5>
                                <h2>what's under the hood?</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/7.png" alt=""></div>
                                    <h5>55 Second Provisioning</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/8.png" alt=""></div>
                                    <h5>ssd hard drives</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/9.png" alt=""></div>
                                    <h5>simple control panel</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/10.png" alt=""></div>
                                    <h5>kvm virtualization</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/11.png" alt=""></div>
                                    <h5>tier-1 bandwidth</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                                <div class="col-sm-4 under_hood">
                                    <div class="row icon m0"><img src="images/icons/feature/12.png" alt=""></div>
                                    <h5>amazing hardware</h5>
                                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget </p>
                                </div> <!--Under Hood-->
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row3 row m0">
                        <div class="container">
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>distros &amp; 1 click apps</h4>
                                    <h3>private networking</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/13.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>Technology</h4>
                                    <h3>simple api</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/14.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                            <div class="col-sm-4 tech_feature">
                                <div class="inner row m0">
                                    <h4>Technology</h4>
                                    <h3>ipv6 support</h3>
                                    <div class="icon m0 row"><img src="images/icons/feature/15.png" alt=""></div>
                                    <a href="#" class="btn btn-primary">learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content_row content_row4 row m0">
                        <div class="container">
                            <div class="row sectionTitle">
                                <h5>distros &amp; 1 click apps</h5>
                                <h2>transfer globally</h2>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 img_part">
                                    <img src="images/icons/feature/6.png" alt="">
                                </div>
                                <div class="col-sm-6">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. </p>
                                    <a href="#" class="btn btn-primary">get started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <footer class="row">
        <div class="top_footer row m0">
            <div class="container">
                <div class="row m0 quick_contact">
                    <ul class="nav nav-pills">
                        <li><a href="tel:1234567890"><i class="lnr lnr-phone"></i>123 - 456 - 7890</a></li>
                        <li><a href="mailto:info@hostpress.com"><i class="lnr lnr-envelope"></i>info@hostpress.com</a></li>
                        <li><a href="#"><i class="lnr lnr-bubble"></i>chat with us</a></li>
                    </ul>
                </div>
                <div class="row shortKnowledge">
                    <div class="col-sm-6 about">
                        <h4><a href="index.html"><img src="images/logo2.png" alt=""></a></h4>
                        <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit.viverra tellus. Vivamus finibus, quam vitae pulvinar euismod, Lorem ipsum dolor sit amet, ectetur adipiscing elit.ectetur adipiscing elit.viverra tellusivamus finibus, quam vitae pulvinar euismod,</p>
                    </div>
                    <div class="col-sm-6 product">
                        <h4>Product</h4>
                        <ul class="product_list nav">
                            <li><a href="#">Webhosting</a></li>
                            <li><a href="#">Reseler Hosting</a></li>
                            <li><a href="#">VPS Hosting</a></li>
                            <li><a href="#">Wordpress Hosting</a></li>
                            <li><a href="hosting-dedicated.html">dedicated hosting</a></li>
                            <li><a href="#">Windows Hosting</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row beInContact m0">
                    <div class="btn-group country_select">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="flag"><img src="images/icons/footer/flag.png" alt=""></span>India<i class="lnr lnr-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">India</a></li>
                            <li><a href="#">Bangladesh</a></li>
                            <li><a href="#">England</a></li>
                            <li><a href="#">Iran</a></li>
                            <li><a href="#">Syria</a></li>
                        </ul>
                    </div>
                    <div class="social_icos">
                        <ul class="nav">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <div class="subscribe_form">
                        <form action="#" class="form-inline">
                            <div class="form-group">
                                <label>Subscribe<small>Newsletter</small></label>
                                <div class="input-group">
                                    <input type="email" class="form-control" placeholder="Email Address">
                                    <span class="input-group-addon"><input type="submit" value="Go" class="btn"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m0 copyright_line">
            Copyright &copy; 1999 - 2015 hostpress.com. All Rights Reserved
        </div>
    </footer>
    
    <!--jQuery, Bootstrap and other vendor JS-->
    
    <!--jQuery-->
    <script src="js/jquery-2.1.4.min.js"></script>
    
    <!--Bootstrap JS-->
    <script src="js/bootstrap.min.js"></script>
    
    <!--Bootstrap Select-->
    <script src="vendors/bootstrap-select/js/bootstrap-select.min.js"></script>
    
    <!--Owl Carousel-->
    <script src="vendors/owl.carousel/owl.carousel.min.js"></script>
    
    <!--Waypoints-->
    <script src="vendors/waypoints/waypoints.min.js"></script>
    
    <!--Counter Up-->
    <script src="vendors/counterup/jquery.counterup.min.js"></script>
    
    <!--Magnific Popup-->
    <script src="js/jquery.magnific-popup.min.js"></script>
    
    <!--Isotope-->
    <script src="vendors/isotope/isotope.min.js"></script>
    
    <!--Isotope-->
    <script src="vendors/infinitescrol/jquery.infinitescroll.min.js"></script>
    
    <!--Theme JS-->
    <script src="js/theme.js"></script>
</body>
</html>