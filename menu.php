
<nav class="navbar navbar-default navbar-static-top fluid_header centered">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt=""></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_navigation" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main_navigation">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php" class="dropdown-toggle" role="button" aria-haspopup="false" aria-expanded="false">Ana Sayfa</a>
                    </li>
                    <li><a href="about-us.php">Hakkımızda</a></li>
                    <li><a href="portfolio.php">Hizmetlerimiz</a></li>
                    <li class="dropdown mega-drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="false" aria-expanded="false">hosting</a>
                        <ul class="dropdown-menu hosting-dropdown mega-menu">
                            <li class="service_list">
                                <div class="fleft service service1">
                                    <div class="media">
                                        <div class="media-left"><a href="#"><img src="images/icons/menu/hosting/1.png" alt=""></a></div>
                                        <div class="media-body">
                                            <a href="hosting-dedicated.php">dedicated hosting</a>
                                            <p>Adipiscing integer act lacinia ed justo.</p>
                                        </div>
                                    </div>
                                </div> <!--Menu Feature-->
                                <div class="fleft service service2">
                                    <div class="media">
                                        <div class="media-left"><a href="#"><img src="images/icons/menu/hosting/2.png" alt=""></a></div>
                                        <div class="media-body">
                                            <a href="hosting-shared.php">shared hosting</a>
                                            <p>Lacus vivamus place turpis elit.</p>
                                        </div>
                                    </div>
                                </div> <!--Menu Feature-->
                                <div class="fleft service service3">
                                    <div class="media">
                                        <div class="media-left"><a href="#"><img src="images/icons/menu/hosting/3.png" alt=""></a></div>
                                        <div class="media-body">
                                            <a href="hosting-reseller.php">reseller hosting</a>
                                            <p>Tristique sit nunc non elit act ultricies.</p>
                                        </div>
                                    </div>
                                </div> <!--Menu Feature-->
                                <div class="fleft service service4">
                                    <div class="media">
                                        <div class="media-left"><a href="#"><img src="images/icons/menu/hosting/4.png" alt=""></a></div>
                                        <div class="media-body">
                                            <a href="domain-transfer.php">domain transfer</a>
                                            <p>Dolor sit amet conse adipiscing orci.</p>
                                        </div>
                                    </div>
                                </div> <!--Menu Feature-->
                            </li>
                            <li class="start_offer">
                                <div class="row m0 inner">
                                    <h3 class="title">Starting@</h3>
                                    <h3 class="offered_price"><small>$</small>0.75/mo</h3>
                                    <h5 class="regular_price"><small>$</small>1.75/m0</h5>
                                    <a href="#" class="btn btn-default">start today</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="false" aria-expanded="false">blog</a>
                        <ul class="dropdown-menu">
                            <li><a href="blog.php">blog</a></li>
                            <li><a href="single.php">blog details</a></li>
                        </ul>
                    </li>
                    <li><a href="contact.php">contact</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>