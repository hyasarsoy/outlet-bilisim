<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hosting Press - VPS Hosting</title>
    
    <!--Favicons-->
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#e74c3c">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#e74c3c">
    
    <!--Bootstrap-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    
    <!--Loader-->
    <link rel="stylesheet" href="css/spinners.css">    
    
    <!-- Vendors -->
    <link rel="stylesheet" href="vendors/owl.carousel/owl.carousel.css">
    <link rel="stylesheet" href="vendors/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="vendors/bootstrap-select/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/lineariconsFree/style.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    
    <!--Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100italic,100,300,300italic,700,400italic,700italic,900,900italic' rel='stylesheet' type='text/css'>
    
    <!--Theme Styles-->
    <link rel="stylesheet" href="css/default/style.css">
    <link rel="stylesheet" href="css/responsive/responsive.css">
    
    <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
    
</head>
<body class="shortcode">
    
    <div class="preloader">
        <div class="dots-loader">Loading ...</div>
    </div>
   
    <?php include 'header.php'; ?>

    <?php include 'menu.php'; ?>
    
    
    <section class="row page_header">
        <div class="container">
            <h3>blog</h3>
            <ol class="breadcrumb">
                <li><a href="index.html">home</a></li>
                <li class="active">blog</li>
            </ol>
        </div>
    </section>
    
    <section class="row blog_content">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 blogs">
                    <div class="row m0 blog">
                        <div class="row m0 image"><a href="single.html"><img src="images/posts/blog/1.jpg" alt=""></a></div>
                        <ul class="blog_infos nav">
                            <li><a href="#"><i class="fa fa-calendar"></i>19 May, 2015</a></li>
                            <li><a href="#"><i class="fa fa-comment"></i>(3) comments</a></li>
                        </ul>
                        <h3><a href="single.html">image post for vps hosting</a></h3>
                        <p>Lorem ipsums dolor sit amet consectetur adipiscing elit integer lacinia malesuada justo vestibulum orci tristique non nunc nonc ultricies enim ut accumsan dolor nullam dapibus rhonus vehicula sed  diam porta tellus nec lacinia lacus vivamus placerat elit.</p>
                        <a href="single.html" class="btn btn-primary">read more</a>
                    </div>
                    <hr class="blog_bottom_line">
                    <div class="row m0 blog">
                        <div class="row m0 image">
                            <div id="gallery-post" class="carousel slide" data-ride="carousel">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <img src="images/posts/blog/2.jpg" alt="...">
                                    </div>
                                    <div class="item">
                                        <img src="images/posts/blog/4.jpg" alt="...">
                                    </div>
                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#gallery-post" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#gallery-post" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <ul class="blog_infos nav">
                            <li><a href="#"><i class="fa fa-calendar"></i>19 May, 2015</a></li>
                            <li><a href="#"><i class="fa fa-comment"></i>(3) comments</a></li>
                        </ul>
                        <h3><a href="single.html">gallery post for vps hosting</a></h3>
                        <p>Lorem ipsums dolor sit amet consectetur adipiscing elit integer lacinia malesuada justo vestibulum orci tristique non nunc nonc ultricies enim ut accumsan dolor nullam dapibus rhonus vehicula sed  diam porta tellus nec lacinia lacus vivamus placerat elit.</p>
                        <a href="single.html" class="btn btn-primary">read more</a>
                    </div>
                    <hr class="blog_bottom_line">
                    <div class="row m0 blog">
                        <div class="row m0 image"><a href="single.html"><img src="images/posts/blog/1.jpg" alt=""><i class="fa fa-youtube-play"></i></a></div>
                        <ul class="blog_infos nav">
                            <li><a href="#"><i class="fa fa-calendar"></i>19 May, 2015</a></li>
                            <li><a href="#"><i class="fa fa-comment"></i>(3) comments</a></li>
                        </ul>
                        <h3><a href="single.html">video post for vps hosting</a></h3>
                        <p>Lorem ipsums dolor sit amet consectetur adipiscing elit integer lacinia malesuada justo vestibulum orci tristique non nunc nonc ultricies enim ut accumsan dolor nullam dapibus rhonus vehicula sed  diam porta tellus nec lacinia lacus vivamus placerat elit.</p>
                        <a href="single.html" class="btn btn-primary">read more</a>
                    </div>
                    <hr class="blog_bottom_line">
                    <div class="row m0 blog quote_blog">
                        <div class="media">
                            <div class="media-left"><i class="fa fa-quote-left"></i></div>
                            <div class="media-body">
                                <p>Donec tristique non neque id eleifend etiamed non eseport ultrices risus nunc laoreet turpis non eleifend felis tortor quis di praesent feugiat nam orci quam tempus vitae erat turpis at.</p>
                                <div class="row m0 quote_writer">- David Hemley</div>
                            </div>
                        </div>
                    </div>
                    <hr class="blog_bottom_line">
                    <div class="row m0 blog">
                        <div class="row m0 image"><a href="single.html"><img src="images/posts/blog/1.jpg" alt=""><i class="fa fa-volume-up"></i></a></div>
                        <ul class="blog_infos nav">
                            <li><a href="#"><i class="fa fa-calendar"></i>19 May, 2015</a></li>
                            <li><a href="#"><i class="fa fa-comment"></i>(3) comments</a></li>
                        </ul>
                        <h3><a href="single.html">audio post for vps hosting</a></h3>
                        <p>Lorem ipsums dolor sit amet consectetur adipiscing elit integer lacinia malesuada justo vestibulum orci tristique non nunc nonc ultricies enim ut accumsan dolor nullam dapibus rhonus vehicula sed  diam porta tellus nec lacinia lacus vivamus placerat elit.</p>
                        <a href="single.html" class="btn btn-primary">read more</a>
                    </div>
                    <hr class="blog_bottom_line">
                    
                    <nav class="pagination_nav">
                        <ul class="pagination">
                            <li><a href="#" aria-label="Previous">previous</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#" aria-label="Next">next</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-sm-4 sidebar">
                    <div class="row m0 inner">
                        <form action="#" class="row m0 search_form">
                            <h4>search</h4>
                            <div class="input-group">
                                <input type="search" class="form-control" placeholder="Search">
                                <span class="input-group-addon"><button type="submit"><i class="fa fa-search"></i></button></span>
                            </div>
                        </form>
                        <hr class="sidebar_line">
                        <div class="row m0 recent_posts">
                            <h4>recent posts</h4>
                            <div class="post media">
                                <div class="media-left">
                                    <a href="single.html"><img src="images/posts/recent/1.jpg" alt=""></a>
                                </div>
                                <div class="media-body">
                                    <h5><a href="single.html">Lorem ipsums dolor sit amet consectetu</a></h5>
                                    <div class="row m0 date"><i class="fa fa-calendar"></i>19 may, 2015</div>
                                </div>
                            </div>
                            <div class="post media">
                                <div class="media-left">
                                    <a href="single.html"><img src="images/posts/recent/2.jpg" alt=""></a>
                                </div>
                                <div class="media-body">
                                    <h5><a href="single.html">Lorem ipsums dolor sit amet consectetu</a></h5>
                                    <div class="row m0 date"><i class="fa fa-calendar"></i>19 may, 2015</div>
                                </div>
                            </div>
                            <div class="post media">
                                <div class="media-left">
                                    <a href="single.html"><img src="images/posts/recent/3.jpg" alt=""></a>
                                </div>
                                <div class="media-body">
                                    <h5><a href="single.html">Lorem ipsums dolor sit amet consectetu</a></h5>
                                    <div class="row m0 date"><i class="fa fa-calendar"></i>19 may, 2015</div>
                                </div>
                            </div>
                        </div>
                        <hr class="sidebar_line">
                        <div class="row m0 categories">
                            <h4>categories</h4>
                            <ul class="nav categories_list">
                                <li><a href="#">Free data analysis <span>(11)</span></a></li>
                                <li><a href="#">Security <span>(16)</span></a></li>
                                <li><a href="#">Free domain transfer <span>(19)</span></a></li>
                                <li><a href="#">Free data analysis <span>(22)</span></a></li>
                                <li><a href="#">Technical support <span>(25)</span></a></li>
                                <li><a href="#">Free web optimization <span>(31)</span></a></li>
                            </ul>
                        </div>
                        <hr class="sidebar_line">
                        <div class="row m0 archives">
                            <h4>archives</h4>
                            <ul class="nav archives_list">
                                <li><a href="#">January <span>(11)</span></a></li>
                                <li><a href="#">February <span>(16)</span></a></li>
                                <li><a href="#">March <span>(19)</span></a></li>
                                <li><a href="#">April <span>(22)</span></a></li>
                                <li><a href="#">May <span>(25)</span></a></li>
                            </ul>
                        </div>
                        <hr class="sidebar_line">
                        <div class="row m0 tags_row">
                            <h4>Tags</h4>
                            <a href="#" class="tag">Vestibul</a>
                            <a href="#" class="tag">Justo temp orci</a>
                            <a href="#" class="tag">Pellent</a>
                            <a href="#" class="tag">Namc</a>
                            <a href="#" class="tag">Consequat elit</a>
                            <a href="#" class="tag">Vestibulum act</a>
                            <a href="#" class="tag">Consequat donctum</a>
                            <a href="#" class="tag">Elit</a>
                            <a href="#" class="tag">Molestie</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <footer class="row">
        <div class="top_footer row m0">
            <div class="container">
                <div class="row m0 quick_contact">
                    <ul class="nav nav-pills">
                        <li><a href="tel:1234567890"><i class="lnr lnr-phone"></i>123 - 456 - 7890</a></li>
                        <li><a href="mailto:info@hostpress.com"><i class="lnr lnr-envelope"></i>info@hostpress.com</a></li>
                        <li><a href="#"><i class="lnr lnr-bubble"></i>chat with us</a></li>
                    </ul>
                </div>
                <div class="row shortKnowledge">
                    <div class="col-sm-6 about">
                        <h4><a href="index.html"><img src="images/logo2.png" alt=""></a></h4>
                        <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit.viverra tellus. Vivamus finibus, quam vitae pulvinar euismod, Lorem ipsum dolor sit amet, ectetur adipiscing elit.ectetur adipiscing elit.viverra tellusivamus finibus, quam vitae pulvinar euismod,</p>
                    </div>
                    <div class="col-sm-6 product">
                        <h4>Product</h4>
                        <ul class="product_list nav">
                            <li><a href="#">Webhosting</a></li>
                            <li><a href="#">Reseler Hosting</a></li>
                            <li><a href="#">VPS Hosting</a></li>
                            <li><a href="#">Wordpress Hosting</a></li>
                            <li><a href="hosting-dedicated.html">dedicated hosting</a></li>
                            <li><a href="#">Windows Hosting</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row beInContact m0">
                    <div class="btn-group country_select">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="flag"><img src="images/icons/footer/flag.png" alt=""></span>India<i class="lnr lnr-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">India</a></li>
                            <li><a href="#">Bangladesh</a></li>
                            <li><a href="#">England</a></li>
                            <li><a href="#">Iran</a></li>
                            <li><a href="#">Syria</a></li>
                        </ul>
                    </div>
                    <div class="social_icos">
                        <ul class="nav">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <div class="subscribe_form">
                        <form action="#" class="form-inline">
                            <div class="form-group">
                                <label>Subscribe<small>Newsletter</small></label>
                                <div class="input-group">
                                    <input type="email" class="form-control" placeholder="Email Address">
                                    <span class="input-group-addon"><input type="submit" value="Go" class="btn"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m0 copyright_line">
            Copyright &copy; 1999 - 2015 hostpress.com. All Rights Reserved
        </div>
    </footer>
    
    <!--jQuery, Bootstrap and other vendor JS-->
    
    <!--jQuery-->
    <script src="js/jquery-2.1.4.min.js"></script>
    
    <!--Bootstrap JS-->
    <script src="js/bootstrap.min.js"></script>
    
    <!--Bootstrap Select-->
    <script src="vendors/bootstrap-select/js/bootstrap-select.min.js"></script>
    
    <!--Owl Carousel-->
    <script src="vendors/owl.carousel/owl.carousel.min.js"></script>
    
    <!--Waypoints-->
    <script src="vendors/waypoints/waypoints.min.js"></script>
    
    <!--Counter Up-->
    <script src="vendors/counterup/jquery.counterup.min.js"></script>
    
    <!--Magnific Popup-->
    <script src="js/jquery.magnific-popup.min.js"></script>
    
    <!--Isotope-->
    <script src="vendors/isotope/isotope.min.js"></script>
    
    <!--Isotope-->
    <script src="vendors/infinitescrol/jquery.infinitescroll.min.js"></script>
    
    <!--Theme JS-->
    <script src="js/theme.js"></script>
</body>
</html>