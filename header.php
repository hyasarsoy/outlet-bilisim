<?php
    require_once("admin/classess/class.user.php");
    $auth_user = new USER();
    
    $stmt = $auth_user->runQuery("SELECT * FROM header WHERE id=1");
    $stmt->execute();
    
    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
    
?>
    <section class="row top_header">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 wc_msg"><?php echo $userRow['slogan']; ?></div>
                  <div class="col-sm-6">
                    <ul class="nav nav-pills">
                        <li><a href="tel:<?php echo $userRow['tel_no']; ?>"><i class="icon-call-out"></i><?php echo $userRow['tel_no']; ?></a></li>
                        <li><a href="mailto:<?php echo $userRow['mail']; ?>"><i class="icon-envelope"></i><?php echo $userRow['mail']; ?></a></li>
                        <li><a href="<?php echo $userRow['facebookurl']; ?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?php echo $userRow['twitterurl']; ?>"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?php echo $userRow['linkedinurl']; ?>"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="<?php echo $userRow['youtubeurl']; ?>"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>