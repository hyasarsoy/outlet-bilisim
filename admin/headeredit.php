<?php
//silence is golden

    require_once("classess/session.php");
    
    require_once("classess/class.user.php");
    $auth_user = new USER();
    
    
    $user_id = $_SESSION['user_session'];
    
    $stmt = $auth_user->runQuery("SELECT * FROM users WHERE user_id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));
    
    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
    
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hoşgeldin - <?php print($userRow['user_email']); ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <?php include 'menu.php'; ?>
    
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Ana Sayfa
                            <small>Düzenleme</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="glyphicon glyphicon-home"></i> Ana Sayfa
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <form method="post" class="form-signin">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Slogan</label>
                            <input class="form-control" placeholder="Sloganınızı Giriniz">
                        </div>
                        <div class="form-group">
                            <label>Telefon Numarası</label>
                            <input class="form-control" placeholder="Telefon Numaranızı Giriniz">
                        </div>
                        <div class="form-group">
                            <label>Mail Adresiniz</label>
                            <input class="form-control" placeholder="Mail Adresinizi Giriniz">
                        </div>
                        <div class="form-group">
                            <label>Facebook Hesabınız Varmı</label>
                            <input class="form-control" placeholder="Facebook linkinizi giriniz">
                        </div>
                        <div class="form-group">
                            <label>Twitter Hesabınız Varmı</label>
                            <input class="form-control" placeholder="Twitter linkinizi giriniz">
                        </div>
                        <div class="form-group">
                            <label>Linkedin Hesabınız Varmı</label>
                            <input class="form-control"  placeholder="Linkedin linkinizi giriniz">
                        </div>
                        <div class="form-group">
                            <label>Youtube Hesabınız Varmı</label>
                            <input class="form-control" placeholder="Youtube linkinizi giriniz">
                        </div>
                    </div>
                        <div class="form-group">
                            <div class="col-sm-offset-7 col-sm-10">
                              <button type="submit" class="btn btn-default">Kaydet</button>
                            </div>
                        </div>
                 
                </div>
                </form>


                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
