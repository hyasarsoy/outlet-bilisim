<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hosting Press - VPS Hosting</title>
    
    <!--Favicons-->
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#e74c3c">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#e74c3c">
    
    <!--Bootstrap-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    
    <!--Loader-->
    <link rel="stylesheet" href="css/spinners.css">
    
    
    <!-- Vendors -->
    <link rel="stylesheet" href="vendors/owl.carousel/owl.carousel.css">
    <link rel="stylesheet" href="vendors/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="vendors/bootstrap-select/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/lineariconsFree/style.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    
    <!--Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    
    <!--Theme Styles-->
    <link rel="stylesheet" href="css/default/style.css">
    <link rel="stylesheet" href="css/responsive/responsive.css">
    
    <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
    
</head>
<body class="shortcode">
    
    <div class="preloader">
        <div class="dots-loader">Loading ...</div>
    </div>
   
    <?php include 'header.php'; ?>
    
    <?php include 'menu.php'; ?>
    

    <section class="row page_header">
        <div class="container">
            <h3>about us</h3>
            <ol class="breadcrumb">
                <li><a href="index.html">home</a></li>
                <li class="active">about us</li>
            </ol>
        </div>
    </section>
    
    <section class="row about_us_content">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <img src="images/about-us/1.jpg" alt="" class="img-responsive">
                </div>
                <div class="col-sm-7">
                    <div class="row sectionTitle text-left">
                        <h5>about us &amp;</h5>
                        <h2>who we</h2>
                    </div>
                    <h4>We Keeps Your Website Up and Running</h4>
                    <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit. Nullam eget llam eget viverra tellus. Vivamus finibus, quam vitae pulvinar euisviverra tellus. s finibus, quam vitae pulvinar euismod, </p>
                    <hr>
                    <h4>Web Hosting Made Easy Now !</h4>
                    <div class="row m0">
                        <ul class="hostpressUnList">
                            <li>domain registration</li>
                            <li>all type web hosting</li>
                            <li>website builder</li>
                        </ul>
                    </div>
                    <p>Dapibus rhonus vehicula sed diam porta tellus nec ac lacini lacus vivamus placerat elit adipiscing elit intege malesuada.</p>
                </div>
            </div>
        </div>
    </section>
    
    
    
    <section class="row clients about_clients">
        <div class="container">
            <div class="row sectionTitle">
                <h5>our partners &amp;</h5>
                <h2>clients</h2>
            </div>
            <div class="row">
                <ul class="nav nav-justified">
                    <li><a href="#" target="_blank"><img src="images/clients/1.png" alt=""></a></li>
                    <li><a href="#" target="_blank"><img src="images/clients/2.png" alt=""></a></li>
                    <li><a href="#" target="_blank"><img src="images/clients/3.png" alt=""></a></li>
                    <li><a href="#" target="_blank"><img src="images/clients/4.png" alt=""></a></li>
                    <li><a href="#" target="_blank"><img src="images/clients/5.png" alt=""></a></li>
                    <li><a href="#" target="_blank"><img src="images/clients/6.png" alt=""></a></li>
                    <li><a href="#" target="_blank"><img src="images/clients/7.png" alt=""></a></li>
                </ul>
            </div>
        </div>
    </section>
    
    <section class="row facts about_us_facts">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 fact"><strong class="counter">5000</strong>Happy clients</div>
                <div class="col-sm-3 fact"><strong class="counter">3000</strong>servers</div>
                <div class="col-sm-3 fact"><strong class="counter">200</strong>dedicated staff</div>
                <div class="col-sm-3 fact"><strong class="counter">20</strong>awards won</div>
            </div>
        </div>
    </section>
    
    <section class="row team_section">
        <div class="container">
            <div class="row sectionTitle">
                <h5>supported staff &amp;</h5>
                <h2>our team</h2>
            </div>
            <div class="row">
                <div class="col-sm-4 team_member">
                    <div class="row m0 inner">
                        <div class="row m0 image"><img src="images/team/1.png" alt=""></div>
                        <h4 class="title">John Glemean</h4>
                        <h6 class="desig">founder &amp; director</h6>
                        <p class="nState">Vivamus finibus quam vitae pulvina adipiscing elit.</p>
                        <p class="hState">Vivamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.</p>
                        <ul class="list-unstyled">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 team_member">
                    <div class="row m0 inner">
                        <div class="row m0 image"><img src="images/team/6.png" alt=""></div>
                        <h4 class="title">Lina Ray</h4>
                        <h6 class="desig">ceo</h6>
                        <p class="nState">Vivamus finibus quam vitae pulvina adipiscing elit.</p>
                        <p class="hState">Vivamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.</p>
                        <ul class="list-unstyled">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 team_member">
                    <div class="row m0 inner">
                        <div class="row m0 image"><img src="images/team/2.png" alt=""></div>
                        <h4 class="title">steave hoffel</h4>
                        <h6 class="desig">head-manager</h6>
                        <p class="nState">Vivamus finibus quam vitae pulvina adipiscing elit.</p>
                        <p class="hState">Vivamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.</p>
                        <ul class="list-unstyled">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 team_member">
                    <div class="row m0 inner">
                        <div class="row m0 image"><img src="images/team/3.png" alt=""></div>
                        <h4 class="title">seaff cafel</h4>
                        <h6 class="desig">manager</h6>
                        <p class="nState">Vivamus finibus quam vitae pulvina adipiscing elit.</p>
                        <p class="hState">Vivamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.</p>
                        <ul class="list-unstyled">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 team_member">
                    <div class="row m0 inner">
                        <div class="row m0 image"><img src="images/team/4.png" alt=""></div>
                        <h4 class="title">vicky dorren</h4>
                        <h6 class="desig">manager</h6>
                        <p class="nState">Vivamus finibus quam vitae pulvina adipiscing elit.</p>
                        <p class="hState">Vivamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.</p>
                        <ul class="list-unstyled">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4 team_member">
                    <div class="row m0 inner">
                        <div class="row m0 image"><img src="images/team/5.png" alt=""></div>
                        <h4 class="title">missel hen</h4>
                        <h6 class="desig">supersior</h6>
                        <p class="nState">Vivamus finibus quam vitae pulvina adipiscing elit.</p>
                        <p class="hState">Vivamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.vamus finibus quam vitae pulvina adipiscing elit.</p>
                        <ul class="list-unstyled">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <footer class="row">
        <div class="top_footer row m0">
            <div class="container">
                <div class="row m0 quick_contact">
                    <ul class="nav nav-pills">
                        <li><a href="tel:1234567890"><i class="lnr lnr-phone"></i>123 - 456 - 7890</a></li>
                        <li><a href="mailto:info@hostpress.com"><i class="lnr lnr-envelope"></i>info@hostpress.com</a></li>
                        <li><a href="#"><i class="lnr lnr-bubble"></i>chat with us</a></li>
                    </ul>
                </div>
                <div class="row shortKnowledge">
                    <div class="col-sm-6 about">
                        <h4><a href="index.html"><img src="images/logo2.png" alt=""></a></h4>
                        <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit.viverra tellus. Vivamus finibus, quam vitae pulvinar euismod, Lorem ipsum dolor sit amet, ectetur adipiscing elit.ectetur adipiscing elit.viverra tellusivamus finibus, quam vitae pulvinar euismod,</p>
                    </div>
                    <div class="col-sm-6 product">
                        <h4>Product</h4>
                        <ul class="product_list nav">
                            <li><a href="#">Webhosting</a></li>
                            <li><a href="#">Reseler Hosting</a></li>
                            <li><a href="#">VPS Hosting</a></li>
                            <li><a href="#">Wordpress Hosting</a></li>
                            <li><a href="hosting-dedicated.html">dedicated hosting</a></li>
                            <li><a href="#">Windows Hosting</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row beInContact m0">
                    <div class="btn-group country_select">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="flag"><img src="images/icons/footer/flag.png" alt=""></span>India<i class="lnr lnr-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">India</a></li>
                            <li><a href="#">Bangladesh</a></li>
                            <li><a href="#">England</a></li>
                            <li><a href="#">Iran</a></li>
                            <li><a href="#">Syria</a></li>
                        </ul>
                    </div>
                    <div class="social_icos">
                        <ul class="nav">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <div class="subscribe_form">
                        <form action="#" class="form-inline">
                            <div class="form-group">
                                <label>Subscribe<small>Newsletter</small></label>
                                <div class="input-group">
                                    <input type="email" class="form-control" placeholder="Email Address">
                                    <span class="input-group-addon"><input type="submit" value="Go" class="btn"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m0 copyright_line">
            Copyright &copy; 1999 - 2015 hostpress.com. All Rights Reserved
        </div>
    </footer>
    
    <!--jQuery, Bootstrap and other vendor JS-->
    
    <!--jQuery-->
    <script src="js/jquery-2.1.4.min.js"></script>
    
    <!--Bootstrap JS-->
    <script src="js/bootstrap.min.js"></script>
    
    <!--Bootstrap Select-->
    <script src="vendors/bootstrap-select/js/bootstrap-select.min.js"></script>
    
    <!--Owl Carousel-->
    <script src="vendors/owl.carousel/owl.carousel.min.js"></script>
    
    <!--Waypoints-->
    <script src="vendors/waypoints/waypoints.min.js"></script>
    
    <!--Counter Up-->
    <script src="vendors/counterup/jquery.counterup.min.js"></script>
    
    <!--Magnific Popup-->
    <script src="js/jquery.magnific-popup.min.js"></script>
    
    <!--Isotope-->
    <script src="vendors/isotope/isotope.min.js"></script>
    
    <!--Isotope-->
    <script src="vendors/infinitescrol/jquery.infinitescroll.min.js"></script>
    
    <!--Theme JS-->
    <script src="js/theme.js"></script>
</body>
</html>