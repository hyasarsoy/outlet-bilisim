<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hosting Press - VPS Hosting</title>
    
    <!--Favicons-->
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#e74c3c">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#e74c3c">
    
    <!--Bootstrap-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    
    <!--Loader-->
    <link rel="stylesheet" href="css/spinners.css">
    
    
    <!-- Vendors -->
    <link rel="stylesheet" href="vendors/owl.carousel/owl.carousel.css">
    <link rel="stylesheet" href="vendors/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="vendors/bootstrap-select/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/lineariconsFree/style.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    
    <!--Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    
    <!--Theme Styles-->
    <link rel="stylesheet" href="css/default/style.css">
    <link rel="stylesheet" href="css/responsive/responsive.css">
    
    <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
    
</head>
<body class="shortcode">
    
    <div class="preloader">
        <div class="dots-loader">Loading ...</div>
    </div>

    <?php include 'header.php'; ?>

    <?php include 'menu.php'; ?>
    
    
    <section class="row page_header">
        <div class="container">
            <h3>Pricing Plans</h3>
            <ol class="breadcrumb">
                <li><a href="index.html">home</a></li>
                <li class="active">Pricing plans</li>
            </ol>
        </div>
    </section>
    <section class="row pricing_plan_table">
        <div class="container">
            <div class="row sectionTitle">
                <h5>our package &amp;</h5>
                <h2>pricing plan</h2>
            </div>
            <div class="table-responsive">
                <div class="col-sm-3 pricing_plan_cell">
                    <div class="row plan_type">dedicated<br>hosting</div>
                    <div class="row pricing_row">pricing</div>
                    <div class="row nav_a">
                        <ul class="nav">
                            <li>Number of emails</li>
                            <li>Number of website</li>
                            <li>Disk spance</li>
                            <li>Database backup / Restore</li>
                            <li>DNS Management</li>
                            <li>Free domain for registration</li>
                        </ul>                        
                        <a href="#" class="btn btn-primary visible-none">purchase</a>
                    </div>
                </div>
                <div class="col-sm-3 pricing_plan_cell">
                    <div class="row plan_type silver">silver plan</div>
                    <div class="row pricing_row">
                        <div class="row m0 price">
                            <span class="currencyType">$</span>
                            <span class="amount">13</span>
                            <small>/mo</small>
                        </div>
                    </div>
                    <div class="row nav_a">
                        <ul class="nav">
                            <li>10 Email Account</li>
                            <li>03 Website Layout</li>
                            <li>100GB</li>
                            <li><i class="fa fa-check"></i></li>
                            <li><i class="fa fa-times"></i></li>
                            <li><i class="fa fa-times"></i></li>
                        </ul>
                        <a href="#" class="btn btn-primary">purchase</a>
                    </div>                    
                </div>
                <div class="col-sm-3 pricing_plan_cell">
                    <div class="row plan_type gold">gold plan <span>popular</span></div>
                    <div class="row pricing_row">
                        <div class="row m0 price">
                            <span class="currencyType">$</span>
                            <span class="amount">17</span>
                            <small>/mo</small>
                        </div>
                    </div>
                    <div class="row nav_a">
                        <ul class="nav">
                            <li>10 Email Account</li>
                            <li>03 Website Layout</li>
                            <li>100GB</li>
                            <li><i class="fa fa-check"></i></li>
                            <li><i class="fa fa-check"></i></li>
                            <li><i class="fa fa-times"></i></li>
                        </ul>
                        <a href="#" class="btn btn-primary">purchase</a>
                    </div>
                </div>
                <div class="col-sm-3 pricing_plan_cell">
                    <div class="row plan_type diamond">diamond plan</div>
                    <div class="row pricing_row">
                        <div class="row m0 price">
                            <span class="currencyType">$</span>
                            <span class="amount">19</span>
                            <small>/mo</small>
                        </div>
                    </div>
                    <div class="row nav_a">
                        <ul class="nav">
                            <li>10 Email Account</li>
                            <li>03 Website Layout</li>
                            <li>100GB</li>
                            <li><i class="fa fa-check"></i></li>
                            <li><i class="fa fa-check"></i></li>

                            <li><i class="fa fa-check"></i></li>
                        </ul>                        
                        <a href="#" class="btn btn-primary">purchase</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="row pricing pricing2">
        <div class="container">
            <div class="row sectionTitle">
                <h5>our package &amp;</h5>
                <h2>pricing plan</h2>
            </div>
            <div class="row">
                <div class="col-sm-4 price_plan">
                    <div class="row inner m0">
                        <div class="plan_intro">
                            <div class="price row m0">
                                <span class="currencyType">$</span>
                                <span class="amount">19</span>
                                <small>/mo</small>
                            </div>
                            <div class="planType row m0">
                                <h4>starter plan</h4>
                            </div>
                        </div>
                        <div class="row m0 service_provide_row">
                            <ul class="nav service_provide">
                                <li>10GB Bandwidth</li>
                                <li>1TB Storage Space</li>
                                <li>10 Free Sub-Domains</li>
                                <li>Cpanel &amp; FTP</li>
                                <li>SEO</li>
                                <li>99.9% Uptime</li>
                                <li>MySQL Databases</li>
                            </ul>
                            <a href="#" class="btn btn-primary">know more</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 price_plan best_plan">
                    <div class="row inner m0">
                        <div class="plan_intro">
                            <div class="price row m0">
                                <span class="currencyType">$</span>
                                <span class="amount">29</span>
                                <small>/mo</small>
                            </div>
                            <div class="planType row m0">
                                <h4>business plan</h4>
                            </div>
                        </div>
                        <div class="row m0 service_provide_row">
                            <ul class="nav service_provide">
                                <li>50GB Bandwidth</li>
                                <li>5TB Storage Space</li>
                                <li>100 Free Sub-Domains</li>
                                <li>Cpanel &amp; FTP</li>
                                <li>SEO</li>
                                <li>99.9% Uptime</li>
                                <li>MySQL Databases</li>
                            </ul>
                            <a href="#" class="btn btn-primary">know more</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 price_plan">
                    <div class="row inner m0">
                        <div class="plan_intro">
                            <div class="price row m0">
                                <span class="currencyType">$</span>
                                <span class="amount">39</span>
                                <small>/mo</small>
                            </div>
                            <div class="planType row m0">
                                <h4>premium plan</h4>
                            </div>
                        </div>
                        <div class="row m0 service_provide_row">
                            <ul class="nav service_provide">
                                <li>50GB Bandwidth</li>
                                <li>10TB Storage Space</li>
                                <li>500 Free Sub-Domains</li>
                                <li>Cpanel &amp; FTP</li>
                                <li>SEO</li>
                                <li>99.9% Uptime</li>
                                <li>MySQL Databases</li>
                            </ul>
                            <a href="#" class="btn btn-primary">know more</a>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="row pricing_plan_table">
        <div class="container">
            <div class="row sectionTitle">
                <h5>Four package &amp;</h5>
                <h2>pricing plan</h2>
            </div>
            <div class="table-responsive">
                <div class="col-sm-3 pricing_plan_cell">
                    <div class="row plan_type basic">Basic Plan</div>
                    <div class="row pricing_row">
                        <div class="row m0 price">
                            <span class="currencyType">$</span>
                            <span class="amount">10</span>
                            <small>/mo</small>
                        </div>
                    </div>
                    <div class="row nav_a">
                        <ul class="nav">
                            <li>10 Email Account</li>
                            <li>03 Website Layout</li>
                            <li>100GB</li>
                            <li><i class="fa fa-check"></i></li>
                            <li><i class="fa fa-times"></i></li>
                            <li><i class="fa fa-times"></i></li>
                        </ul>
                        <a href="#" class="btn btn-primary">purchase</a>
                    </div>
                </div>
                <div class="col-sm-3 pricing_plan_cell">
                    <div class="row plan_type silver">silver plan</div>
                    <div class="row pricing_row">
                        <div class="row m0 price">
                            <span class="currencyType">$</span>
                            <span class="amount">13</span>
                            <small>/mo</small>
                        </div>
                    </div>
                    <div class="row nav_a">
                        <ul class="nav">
                            <li>10 Email Account</li>
                            <li>03 Website Layout</li>
                            <li>100GB</li>
                            <li><i class="fa fa-check"></i></li>
                            <li><i class="fa fa-times"></i></li>
                            <li><i class="fa fa-times"></i></li>
                        </ul>
                        <a href="#" class="btn btn-primary">purchase</a>
                    </div>                    
                </div>
                <div class="col-sm-3 pricing_plan_cell">
                    <div class="row plan_type gold">gold plan <span>popular</span></div>
                    <div class="row pricing_row">
                        <div class="row m0 price">
                            <span class="currencyType">$</span>
                            <span class="amount">17</span>
                            <small>/mo</small>
                        </div>
                    </div>
                    <div class="row nav_a">
                        <ul class="nav">
                            <li>10 Email Account</li>
                            <li>03 Website Layout</li>
                            <li>100GB</li>
                            <li><i class="fa fa-check"></i></li>
                            <li><i class="fa fa-check"></i></li>
                            <li><i class="fa fa-times"></i></li>
                        </ul>
                        <a href="#" class="btn btn-primary">purchase</a>
                    </div>
                </div>
                <div class="col-sm-3 pricing_plan_cell">
                    <div class="row plan_type diamond">diamond plan</div>
                    <div class="row pricing_row">
                        <div class="row m0 price">
                            <span class="currencyType">$</span>
                            <span class="amount">19</span>
                            <small>/mo</small>
                        </div>
                    </div>
                    <div class="row nav_a">
                        <ul class="nav">
                            <li>10 Email Account</li>
                            <li>03 Website Layout</li>
                            <li>100GB</li>
                            <li><i class="fa fa-check"></i></li>
                            <li><i class="fa fa-check"></i></li>


                            <li><i class="fa fa-check"></i></li>
                        </ul>                        
                        <a href="#" class="btn btn-primary">purchase</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="row pricing">
        <div class="container">
            <div class="row sectionTitle">
                <h5>our package &amp;</h5>
                <h2>pricing plan</h2>
            </div>   
            <div class="owl-carousel pricing_plan">
                <div class="item">
                    <div class="row m0 plan">
                        <div class="price row m0">
                            <span class="currencyType">$</span>
                            <span class="amount">19</span>
                            <small>/mo</small>
                        </div>
                        <div class="serviceType row m0">
                            <h4>dedicated hosting</h4>
                        </div>
                        <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit.</p>
                        <a href="#" class="btn btn-primary">know more</a>
                    </div>
                </div>
                <div class="item">
                    <div class="row m0 plan">
                        <div class="price row m0">
                            <span class="currencyType">$</span>
                            <span class="amount">29</span>
                            <small>/mo</small>
                        </div>
                        <div class="serviceType row m0">
                            <h4>VPS hosting</h4>
                        </div>
                        <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit.</p>
                        <a href="#" class="btn btn-primary">know more</a>
                    </div>
                </div>
                <div class="item">
                    <div class="row m0 plan">
                        <div class="price row m0">
                            <span class="currencyType">$</span>
                            <span class="amount">39</span>
                            <small>/mo</small>
                        </div>
                        <div class="serviceType row m0">
                            <h4>shared hosting</h4>
                        </div>
                        <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit.</p>
                        <a href="#" class="btn btn-primary">know more</a>
                    </div>
                </div>
                <div class="item">
                    <div class="row m0 plan">
                        <div class="price row m0">
                            <span class="currencyType">$</span>
                            <span class="amount">49</span>
                            <small>/mo</small>
                        </div>
                        <div class="serviceType row m0">
                            <h4>reseller hosting</h4>
                        </div>
                        <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit.</p>
                        <a href="#" class="btn btn-primary">know more</a>
                    </div>
                </div>
            </div>        
            <div class="row pricing_nav">
                <div class="col-sm-4 col-sm-offset-4">
                    <div id="pricing_nav" class="row m0"></div>
                </div>
            </div>
        </div>
    </section>
     <section class="row pricing_bottom"></section>
     
     
    <footer class="row">
        <div class="top_footer row m0">
            <div class="container">
                <div class="row m0 quick_contact">
                    <ul class="nav nav-pills">
                        <li><a href="tel:1234567890"><i class="lnr lnr-phone"></i>123 - 456 - 7890</a></li>
                        <li><a href="mailto:info@hostpress.com"><i class="lnr lnr-envelope"></i>info@hostpress.com</a></li>
                        <li><a href="#"><i class="lnr lnr-bubble"></i>chat with us</a></li>
                    </ul>
                </div>
                <div class="row shortKnowledge">
                    <div class="col-sm-6 about">
                        <h4><a href="index.html"><img src="images/logo2.png" alt=""></a></h4>
                        <p>Lorem ipsum dolor sit amet, ectetur adipiscing elit.viverra tellus. Vivamus finibus, quam vitae pulvinar euismod, Lorem ipsum dolor sit amet, ectetur adipiscing elit.ectetur adipiscing elit.viverra tellusivamus finibus, quam vitae pulvinar euismod,</p>
                    </div>
                    <div class="col-sm-6 product">
                        <h4>Product</h4>
                        <ul class="product_list nav">
                            <li><a href="#">Webhosting</a></li>
                            <li><a href="#">Reseler Hosting</a></li>
                            <li><a href="#">VPS Hosting</a></li>
                            <li><a href="#">Wordpress Hosting</a></li>
                            <li><a href="hosting-dedicated.html">dedicated hosting</a></li>
                            <li><a href="#">Windows Hosting</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row beInContact m0">
                    <div class="btn-group country_select">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="flag"><img src="images/icons/footer/flag.png" alt=""></span>India<i class="lnr lnr-chevron-down"></i>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">India</a></li>
                            <li><a href="#">Bangladesh</a></li>
                            <li><a href="#">England</a></li>
                            <li><a href="#">Iran</a></li>
                            <li><a href="#">Syria</a></li>
                        </ul>
                    </div>
                    <div class="social_icos">
                        <ul class="nav">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <div class="subscribe_form">
                        <form action="#" class="form-inline">
                            <div class="form-group">
                                <label>Subscribe<small>Newsletter</small></label>
                                <div class="input-group">
                                    <input type="email" class="form-control" placeholder="Email Address">
                                    <span class="input-group-addon"><input type="submit" value="Go" class="btn"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m0 copyright_line">
            Copyright &copy; 1999 - 2015 hostpress.com. All Rights Reserved
        </div>
    </footer>
    
    <!--jQuery, Bootstrap and other vendor JS-->
    
    <!--jQuery-->
    <script src="js/jquery-2.1.4.min.js"></script>
    
    <!--Bootstrap JS-->
    <script src="js/bootstrap.min.js"></script>
    
    <!--Bootstrap Select-->
    <script src="vendors/bootstrap-select/js/bootstrap-select.min.js"></script>
    
    <!--Owl Carousel-->
    <script src="vendors/owl.carousel/owl.carousel.min.js"></script>
    
    <!--Waypoints-->
    <script src="vendors/waypoints/waypoints.min.js"></script>
    
    <!--Counter Up-->
    <script src="vendors/counterup/jquery.counterup.min.js"></script>
    
    <!--Magnific Popup-->
    <script src="js/jquery.magnific-popup.min.js"></script>
    
    <!--Isotope-->
    <script src="vendors/isotope/isotope.min.js"></script>
    
    <!--Isotope-->
    <script src="vendors/infinitescrol/jquery.infinitescroll.min.js"></script>
    
    <!--Theme JS-->
    <script src="js/theme.js"></script>
</body>
</html>